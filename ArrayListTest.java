package adt.list;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class ArrayListTest {

	private List<String> list;
	
	@Before
	public void setUp(){
		list = new ArrayList<String>();
	}
	
	
	@Test
	public void testGetMethod(){
		list.add(0, "obj0");
		list.add(1, "obj1");
		list.add(2, "obj2");
		list.add(4, "obj3");
		
		assertEquals("obj0", list.get(0));
		assertEquals("obj1", list.get(1));
		assertEquals("obj2", list.get(2));
		assertEquals("obj3", list.get(3));
		
	}

}